workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DOCS_DIR: documentation

stages:
  #- tests      # unit test
  - check      # check SPoT
  - preprocessing # preprocessing e.g translation of tooltips
  - build      # build SD Ontology artifacts
  - validate   # validate SD samples against SD Ontology
  - doc        # generate documentation
  - package    # generate packages
  - page       # generate landing page

#########################################################
# unittests for toolchain aka CI/CD Pipeline
#########################################################
#toolchain-unittests:
  # run python unit tests for tool chain
  # up-and-running ci/cd pipeline is prerequisite for all other stages
#  stage: tests
#  image: python:3-buster
#  before_script:
#    - cd toolchain
#    - pip install -Ur requirements.txt
#    - pip install -Ur test-requirements.txt
#  script:
#    - python -m unittest

#########################################################
# check Single-Point-of-Truth stage
#########################################################
check-yaml-syntax:
  stage: check
  image: node:15-buster
  before_script:
    - npm install -g ajv-cli
  script:
    - >
      for filepath in $(find single-point-of-truth/yaml -maxdepth 2 -type f -name "*.yaml"); do
        if [[ $filepath != *"validation"* ]] && [[ $filepath != *"to-be-integrated"* ]]; then
          echo ${filepath}
          ajv test -s single-point-of-truth/yaml/validation/schema.json -d $filepath --valid
        fi
      done
  artifacts:
    paths:
      - allYaml/

check-yaml-semantic:
  stage: check
  image: python:3-buster
  before_script:
    - mkdir -p semantic_allYaml/
    - mkdir -p allYaml/
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    #- >
    #  for filepath in $(find ../single-point-of-truth/yaml/* -type d); do
    #    if [[ $filepath != *"to-be-integrated"* ]]; then
    #      cp $filepath/*.yaml ../semantic_allYaml/
    #    fi
    #    if [[ $filepath != *"validation"* ]] && [[ $filepath != *"to-be-integrated"* ]]; then
    #      cp $filepath/*.yaml ../allYaml/
    #    fi
    #  done
    #- >
    - python3 semantic_validation/cli.py semantic-validation -e 'gax-core' -e 'gax-trust-framework'  --srcFile 'pathFile.csv'
    - python3 semantic_validation/cli.py semantic-validation -e 'gax-core' -e 'trusted-cloud'  --srcFile 'pathFile.csv'
  artifacts:
    paths:
      - semantic_allYaml/
    #  - allYaml/
  #only:
  #  changes:
  #    - single-point-of-truth/**/*

#########################################################
# preprocessing
#########################################################

automatic-translation:
  stage: preprocessing
  image: python:3-buster
  before_script:
    - mkdir -p preprocessed_yaml/
    - cd toolchain
    - pip install -Ur requirements.txt
    - pip install ruamel.yaml
    - pip install googletrans==4.0.0-rc1
  script:
    - python3 language_preprocessing/cli.py language_translator -s 'pathFile.csv' -e 'trusted-cloud' -e 'gax-core' -e 'gax-trust-framework' -l 'de' -l 'en' -t "../preprocessed_yaml/"
  artifacts:
    paths:
      - preprocessed_yaml/

#########################################################
# build
#########################################################
create-shacl:
  stage: build
  image: python:3-buster
  before_script:
    - mkdir -p yaml2shacl/
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 yaml2shacl.py gax-core gax-trust-framework trusted-cloud
    - python3 constraintIndex.py
  artifacts:
    paths:
      - yaml2shacl/

#create-json:
#  stage: build
#  image: python:3-buster
#  before_script:
#    - mkdir -p yaml2json/
#    - cd toolchain
#    - pip install -Ur requirements.txt
#  script:
#    - python3 yaml2json.py
#  dependencies:
#    - check-yaml-semantic
#  artifacts:
#    paths:
#      - yaml2json/

create-ontology:
  stage: build
  image: python:3-buster
  before_script:
    - mkdir -p yaml2ontology/
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 ontology_generation/cli.py ontology-generation --srcFile pathFile.csv --dstPath ../yaml2ontology/  -e gax-core -e trusted-cloud -e gax-trust-framework
  dependencies:
    - check-yaml-semantic
  artifacts:
    paths:
      - yaml2ontology/

#########################################################
# validate
#########################################################
check-rdf-shacl:
  # validate SD instances in implementation/instances against
  # shacl shapes build in create-shacl
  stage: validate
  image: python:3-buster
  before_script:
    - cd toolchain
    - pip install -Ur requirements.txt
  script:
    - python3 check_shacl.py gax-core gax-trust-framework trusted-cloud

#########################################################
# build-doc
#########################################################
gen-UML-4-shacl:
  stage: doc
  image: ruby:2.7
  before_script:
    - rm -rf /var/lib/apt/lists/*
    - apt-get update
    - apt-cache gencaches
    - apt-get install -y default-jdk
    - apt-get install -y zip unzip
    - mkdir -p shacl2uml
    - apt install -y graphviz
    - wget https://github.com/sparna-git/shacl-play/releases/download/0.5/shacl-play-app-0.5-onejar.jar -O shacl-play.jar
  script:
    - echo "Create UML Visualization"
    - >
      for filename in yaml2shacl/*.ttl; do
        java -jar shacl-play.jar draw -i "$filename" -o "shacl2uml/$(basename "$filename" .ttl).png"
      done
  dependencies:
    - create-shacl
  artifacts:
    paths:
      - shacl2uml

gen-visualization-4-samples:
  stage: doc
  image: node:15-buster
  script:
    - cd toolchain/visualization
    - chmod +x build.sh
    - ./build.sh
  artifacts:
    paths:
      - toolchain/visualization/output/

gen-widoco-4-ontology:
  stage: doc
  image: ruby:2.7
  before_script:
    - rm -rf /var/lib/apt/lists/*
    - apt-get update
    - apt-cache gencaches
    - apt-get install -y zip unzip
  script:
    - . toolchain/create_widoco_doc.sh
  artifacts:
    paths:
      - widoco

gen-packages:
  stage: package
  image: bash:latest
  script:
    - |
      apk add curl

      mkdir -p shaclPackage
      mkdir -p shaclPackage/gax-core
      mkdir -p shaclPackage/gax-trust-framework
      cp -r yaml2shacl/gax-core/* shaclPackage/gax-core/
      cp -r yaml2shacl/gax-trust-framework/* shaclPackage/gax-trust-framework/

      mkdir -p widocoPackage
      mkdir -p widocoPackage/gax-core
      mkdir -p widocoPackage/gax-trust-framework
      cp -r widoco/gax-core/* widocoPackage/gax-core/
      cp -r widoco/gax-trust-framework/* widocoPackage/gax-trust-framework/

      cd shaclPackage
      tar -czvf shacl.tar.gz *
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file shacl.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/shacl/${CI_COMMIT_TAG}/shacl.${CI_COMMIT_TAG}.tar.gz

      cd ../single-point-of-truth/context
      tar -czvf context.tar.gz *
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file context.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/context/${CI_COMMIT_TAG}/context.${CI_COMMIT_TAG}.tar.gz

      cd ../../widocoPackage
      tar -czvf documentation.tar.gz *
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file documentation.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/documentation/${CI_COMMIT_TAG}/documentation.${CI_COMMIT_TAG}.tar.gz

      cd ../yaml2ontology/gax-ontology-complete
      tar -czvf ontology.tar.gz *
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ontology.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/ontology/${CI_COMMIT_TAG}/ontology.${CI_COMMIT_TAG}.tar.gz

  dependencies:
    - create-shacl
    - gen-widoco-4-ontology
  only:
    - tags



#########################################################
# generate landing page
#########################################################
# copied from gaia-x/toolset/gaia-x-document-template as include will break pipeline
#generate-html:
#  stage: page
#  image: python:3-bullseye
#  before_script:
#    - apt-get update && apt-get -y install zip
#    - git submodule update --recursive --remote
#    - test -f requirements.txt && pip install -Ur requirements.txt
#    - test -f gaia-x-document-template/requirements.txt && pip install -Ur gaia-x-document-template/requirements.txt
#    - pip install gxTrustFrameworkSPoT --extra-index-url https://gitlab.com/api/v4/projects/38506401/packages/pypi/simple --force
#    - cp -r gaia-x-document-template/template_html/extra.css $DOCS_DIR/
#    - cp -r gaia-x-document-template/template_html/extra.js $DOCS_DIR/

    # copy sd-attributes from previous job
#    - cp sd-attributes/sd-attributes.md $DOCS_DIR/


#  script:
#    - test -f generate_html_custom.sh && bash ./generate_html_custom.sh
#    - j2 mkdocs.yml.j2 > mkdocs.yml
#    # rename directory for documentation from 'docs' to 'documentation'
#    - sed -i 's/docs_dir:\ docs/docs_dir:\ documentation/g' mkdocs.yml
#    - mkdocs build
#    - test -f pre_publish_html.sh && bash pre_publish_html.sh
#    - mkdir landing-page
#    - cp -r public/* landing-page

    # copy Widoco to landing page
#    - mkdir landing-page/widoco
#    - cp -r widoco/* landing-page/widoco

    # make sd-artifacts available ob landing page
#    - zip -r landing-page/downloads/shacl.zip yaml2shacl/
#    - zip -r landing-page/downloads/ontology.zip yaml2ontology/
#    - zip -r landing-page/downloads/json.zip yaml2json/

#    - echo "done" # If last command in script is 'test -f somefile', job fails
#  dependencies:
#    - create-shacl
#    - create-json
#    - create-ontology
#    - gen-widoco-4-ontology
#    - gen-tables-4-attributes
#  artifacts:
#    paths:
#      - landing-page
#    expire_in: 1 day

#########################################################
# build GitLab pages
#########################################################
pages:
  stage: .post
  before_script:
    - apt-get update && apt-get -y install zip python3-pip
    - cd toolchain/html
    - pip3 install beautifulsoup4
    - cd ../..
  script:
    - pip3 list
   
    # Create the tagged folders
    - mkdir -p public/$CI_COMMIT_REF_NAME
    - mkdir -p public/$CI_COMMIT_REF_NAME/yaml2ontology
    - mkdir -p public/$CI_COMMIT_REF_NAME/yaml2shacl
#    - mkdir -p public/$CI_COMMIT_REF_NAME/yaml2json
    - mkdir -p public/$CI_COMMIT_REF_NAME/visualization
    - mkdir -p public/$CI_COMMIT_REF_NAME/widoco
    
    # Copy the artifacts to the tagged folders
    - cp -r landing-page/* public/$CI_COMMIT_REF_NAME/
    - cp -r yaml2ontology/* public/$CI_COMMIT_REF_NAME/yaml2ontology
    - cp -r yaml2shacl/* public/$CI_COMMIT_REF_NAME/yaml2shacl
##    - cp -r yaml2json/* public/$CI_COMMIT_REF_NAME/yaml2json
    - cp -r toolchain/visualization/output/* public/$CI_COMMIT_REF_NAME/visualization
    - cp -r widoco/* public/$CI_COMMIT_REF_NAME/widoco
    - cp -r single-point-of-truth/shacl/* public/yaml2shacl

    # publish latest widoco on main page
    - if [ -d "public/widoco" ]; then
        rm -r public/widoco;
      fi
    - mkdir public/widoco
    #- cp -r widoco/* public/widoco

    # Prepare drop box
    - echo "Downloading previous main.zip"
    - mkdir public/packages
    - if curl --output /dev/null --silent --head --fail https://gaia-x.gitlab.io/technical-committee/service-characteristics/packages/main.zip; then
        curl -L https://gaia-x.gitlab.io/technical-committee/service-characteristics/packages/main.zip -o public/packages/main.zip;
      fi

    - if test -f "public/packages/main.zip"; then
        unzip public/packages/main.zip -d .;
        rm public/packages/main.zip;
      fi

    - echo "Packaging and deploying Service Characteristics as main.zip"
    - zip -r public/packages/main.zip public/

    - python3 toolchain/html/mangle_html.py public
    - python3 toolchain/html/generate_redirect.py public 
  only:
    - tags
  #  - main # possible solution for later with tags https://stackoverflow.com/questions/42796018/how-to-run-a-gitlab-ci-yml-job-only-on-a-tagged-branch/52807912#52807912
  artifacts:
    paths:
      - public # do not change this (see https://gitlab.com/gitlab-org/gitlab/-/issues/1719#note_30008931)
